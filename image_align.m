GetExpShift(const Image *img1, const Image *img2, int shift_bits, int
shift_ret[2])
{
int min_err;
int cur_shift[2];
Bitmap tb1, tb2;
Bitmap eb1, eb2;
int i, j;
if (shift_bits > 0) {
Image sml_img1, sml_img2;
ImageShrink2(img1, &sml_img1);
ImageShrink2(img2, &sml_img2);
GetExpShift(&sml_img1, &sml_img2, shift_bits-1, cur_shift);
ImageFree(&sml_img1);
ImageFree(&sml_img2);
cur_shift[0] *= 2;
cur_shift[1] *= 2;
} else
cur_shift[0] = cur_shift[1] = 0;
ComputeBitmaps(img1, &tb1, &eb1);
ComputeBitmaps(img2, &tb2, &eb2);
min_err = img1->xres * img1->yres;
for (i = -1; i < = 1; i++)
for (j = -1; j <= 1; j++) {
int xs = cur_shift[0] + i;
int ys = cur_shift[1] + j;
Bitmap shifted_tb2;
Bitmap shifted_eb2;
Bitmap diff_b;
int err;
BitmapNew(img1->xres, img1->yres, &shifted_tb2);
BitmapNew(img1->xres, img1->yres, &shifted_eb2);
BitmapNew(img1->xres, img1->yres, &diff_b);
BitmapShift(&tb2, xs, ys, &shifted_tb2);
BitmapShift(&eb2, xs, ys, &shifted_eb2);
BitmapXOR(&tb1, &shifted_tb2, &diff_b);
BitmapAND(&diff_b, &eb1, &diff_b);
BitmapAND(&diff_b, &shifted_eb2, &diff_b);
err = BitmapTotal(&diff_b);
if (err < min_err) {
shift_ret[0] = xs;
shift_ret[1] = ys;
min_err = err;
}
BitmapFree(&shifted_tb2);
BitmapFree(&shifted_eb2);
}
BitmapFree(&tb1); BitmapFree(&eb1);
BitmapFree(&tb2); BitmapFree(&eb2);
}
